<!-- HEADIGNS -->
# mi title h1
## mi title h2
### mi title h3
#### mi title h4
##### mi title h5
###### mi title h6

<!-- texto italica -->
this is an *italic* text

<!-- texto strong -->
this is an **strong** text

<!-- texto tachado -->
this is an texto ~~tachado~~

<!-- ul -->
* html
    * html2
* css
* javascript

1. html

2. css
3. javascript


<!-- link -->
[facebook.com](https://www.facebook.com)

[facebook.com](https://www.facebook.com "agregando titulo")

<!-- citas -->
> citas

<!-- lineas -->
---
___

<!-- una linea de codiogo -->
`console.log('hello word)`

<!-- un bloque de codigo -->

``` javascript
const fc = function(a , b){
    return a + b;
}
const valor = fc( 2, 3);
console.log(valor);
```

<!-- tablas y columnas -->
| Tables | Are | cool |
| ------ |:--- |:---- |
| col 3 is|right|10000|
| col 3 is|right|10000|

<!-- imagenes -->
![logo kras](wisftock.png "wisftock")  

* [x] tarea1
* [] tarea2
* [] tarea3
* [x] tarea4
